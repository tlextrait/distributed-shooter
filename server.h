/**
* @file server.h
* @author Thomas Lextrait
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>

#include <pthread.h>

/**
* GameMaster thread
*/
void *GameMaster(void *client_data);

/**
 * Sends given data to given player
 */
int sendDataTo(void* data, size_t size, Player p);

/**
 * Moves given player in the given direction
 */
void movePlayer(Player player, int dir);

/**
 * Makes given player shoot in player's direction
 */
void shoot(Player player);

/**
 * Adds a player to the global list
 */
void addPlayer(Player player);

/**
 * Removes player from global list
 */
void removePlayer(Player player);

/**
 * Removes a bullet from the given player
 */
void removeBullet(Player player, Bullet bullet);

/**
 * Checks if given player's bullet killed another player
 */
int checkBullet(Player player, Bullet bullet);

/**
* printm is a thread-safe printing function
*/
void printm(char* message);

/**
* Print a help message
*/
void help();
