/**
* @file structs.c
* @author Thomas Lextrait
*/

#include "structs.h"
#include "utils.h"

/**
 * Loads a map from the given filename
 */
void loadMap(Map* map, char* filename)
{
    char* line = (char*)malloc(sizeof(char) * MAX_MAP_WIDTH);
    
    FILE* file = (FILE*)malloc(sizeof(FILE));
    int i = 0, len;
    Pos spawnPos;
    
    map->width = 0;
    map->height = 0;
    map->spawnCount = 0;
    map->wallCount = 0;
    
    if(pathExists(filename)>=0){
        
        file = fopen(filename, "r");
        
        while(fgets(line, MAX_MAP_WIDTH, file)){
            len = strlen(line);
            if(len > 2){
                // Trim new line
                if(line[len-1] == '\n'){line[len-1] = '\0';}
                
                strcpy(map->map[map->height], line);
                
                // analyze map line
                map->width = len;
                for(i=0; i<len; i++){
                    switch(line[i]){
                        case ' ':
                            spawnPos.x = i;
                            spawnPos.y = map->height;
                            map->spawn[map->spawnCount] = spawnPos;
                            map->spawnCount++;
                            break;
                        case '#':
                            map->wallCount++;
                            break;
                        default:
                            break;
                    }
                }
                map->height++;
            }
        }
    }
}

/**
 * Prints a map to screen
 */
void printMap(Map map)
{
    int i, a, closer = 0;
    
    // Place the players and their bullets into the map
    for(i=0; i<map.playerCount; i++){
        map.map[map.players[i].pos.y][map.players[i].pos.x] = map.players[i].symbol;
        
        for(a=0; a<map.players[i].bulletCount; a++){
            map.map[map.players[i].bullets[a].pos.y][map.players[i].bullets[a].pos.x] = ELEMENT_BULLET;
        }
    }
    
    for(i=0; i<map.height; i++){
        printf("%s", map.map[i]);
        
        if(i==0 || i==2){
            printc("  +-------------------------------+", GREEN, NOSTYLE);
        }else if(i==1){
            printc("   PLAYERS", GREEN, NOSTYLE);
        }else if(i-2 > map.playerCount && !closer){
            printc("  +-------------------------------+", GREEN, NOSTYLE);
            closer = 1;
        }else if(i-2 <= map.playerCount && i-2 > 0){
            printc("   [", CYAN, NOSTYLE);
            printc(&(map.players[i-3].symbol), CYAN, NOSTYLE);
            printc("] ", CYAN, NOSTYLE);
            printc(map.players[i-3].nickname, CYAN, NOSTYLE);
            printc(" $", CYAN, NOSTYLE);
            printc(itoa(map.players[i-3].score), CYAN, NOSTYLE);
            printc(" %", CYAN, NOSTYLE);
            printc(itoa(map.players[i-3].health), CYAN, NOSTYLE);
            printc(" *", CYAN, NOSTYLE);
            printc(itoa(map.players[i-3].ammo), CYAN, NOSTYLE);
        }
        
        printf("\n");
    }
    printf("%s\n", GAME_DIRECTIONS);
}

/**
 * Returns 1 if this is a valid spawning location
 */
int isSpawnPos(Map map, Pos pos)
{
    return !isOccupied(map, pos) && isValidPos(map, pos);
}

/**
 * Returns 1 if a player or bullet is at given location
 * -1 if invalid location
 */
int isOccupied(Map map, Pos pos)
{
    int i, a;
    
    // check position is valid
    if(!isValidPos(map, pos)){return -1;}
    
    // check all player's positions (not efficient...)
    for(i=0; i<map.playerCount; i++){
        // check player[i]
        if(map.players[i].pos.x == pos.x &&
           map.players[i].pos.y == pos.y){
            return 1;
        }
        // check all of player[i]'s bullet locations
        for(a=0; a<map.players[i].bulletCount; a++){
            if(map.players[i].bullets[a].pos.x == pos.x &&
               map.players[i].bullets[a].pos.y == pos.y){
                return 1;
            }
        }
    }
    return 0;
}

/**
 * Returns 1 if given location is a valid moving position
 * (not a wall)
 */
int isValidPos(Map map, Pos pos)
{
    return pos.x >= 0 &&
            pos.y >= 0 &&
            pos.x < map.width &&
            pos.y < map.height &&
            map.map[pos.y][pos.x] != '#';
}

/**
 * Returns 1 if the given positions are equivalent
 */
int samePos(Pos p1, Pos p2)
{
    return p1.x == p2.x && p1.y == p2.y;
}
