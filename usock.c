/**
 * @file usock.c
 * @author Thomas Lextrait, tlextrait@wpi.edu
 */

#include "usock.h"

/**
 * Create and return a listening UDP socket
 */
int udp_create_rcv(int port)
{
    int sock;
    struct sockaddr_in server_addr;
    
    if((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
        perror("Couldn't create socket");
        exit(1);
    }
    
    memset((char *) &server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    bzero(&(server_addr.sin_zero),8);
    
    if(bind(sock, (struct sockaddr *)&server_addr, sizeof(server_addr))==-1){
        perror("Couldn't bind socket");
        exit(1);
    }
    
    return sock;
}

/**
 * Destroys given UDP socket
 */
int udp_destroy(int sock)
{
    return (close(sock));
}

/**
 * Receive data from a UDP socket
 * @return bytes read
 */
int udp_rcv(int sock, void* data, size_t max_length, char* client_ip)
{
    struct sockaddr_in client_addr;
    unsigned int addr_len = sizeof(struct sockaddr);
    
    int bytes = recvfrom(sock, data, max_length, 0, 
             (struct sockaddr *)&client_addr, &addr_len);
    
    if(client_ip != NULL){
        strcpy(client_ip, inet_ntoa(client_addr.sin_addr));
    }
    
    return bytes;
}

/**
 * Send data via given UDP socket
 * @return number of bytes sent
 */
int udp_snd(void* data, size_t length, int port, char* address)
{
    int sock, bytes;
    struct sockaddr_in server_addr;
    struct hostent *host;
    
    host = (struct hostent *) gethostbyname(address);
    
    if((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){
        perror("Couldn't create socket");
        exit(1);
    }
    
    memset((char *) &server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr = *((struct in_addr *)host->h_addr);
    bzero(&(server_addr.sin_zero),8);
    
    if(inet_aton(address, &server_addr.sin_addr)==0) {
        perror("inet_aton() failed");
        exit(1);
    }
    
    bytes = sendto(sock, data, length, 0,
           (struct sockaddr *)&server_addr, sizeof(struct sockaddr));
    
    if(bytes < 0){
        perror("Couldn't send data");
        exit(1);
    }
    
    udp_destroy(sock);
    
    return bytes;
}
