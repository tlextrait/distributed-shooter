/**
* @file client.c
* @author Thomas Lextrait
*/

#include "usock.h"
#include "structs.h"
#include "client.h"
#include "utils.h"

#define MAX_INPUT_LENGTH		128
#define DEFAULT_ADDRESS         "192.168.1.109"
#define DEFAULT_PORT            6666

#define CLIENT_PORT             7777

/**
* GLOBAL VARIABLES
*/
Player me;
Map map;
int specifyServer = 0;
char server_address[20];
int specifyPort = 0;
int server_port;
int killClient = 0;

pthread_t       keyMaster;       // keyMaster thread
pthread_attr_t 	attr;            // thread attributes

int main(int argc, char* argv[]){
	
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	// Option stuff
	int c;
	extern int optind, opterr;
	extern char *optarg;
    
    // Socket stuff
    Message msg;
    int sock;
    
    // Thread stuff
	int rc;
    
    // Termios stuff
    struct termios orig_term_attr;
    struct termios new_term_attr;
    
    srand(time(NULL));
    
	/* ------------------------------------------------------------------------ */
	
	// Parse command line options
	while((c = getopt(argc, argv, "hka:p:")) != EOF) {
	    switch(c){
            case 'a': // specify address
                strcpy(server_address, optarg);
                specifyServer = 1;
                break;
            case 'p': // specify port
                server_port = atoi(optarg);
                specifyPort = 1;
                break;
			case 'k': // kill server
                msg.action = ACTION_SHUT;
                udp_snd(&msg, sizeof(Message), DEFAULT_PORT, DEFAULT_ADDRESS);
                exit(0);
			default:
		    case '?': // opt error
		    case 'h': // help
		      	help();
				exit(1);
	    }
  	}
    
    if(!specifyServer){
        strcpy(server_address, DEFAULT_ADDRESS);
    }
    if(!specifyPort){
        server_port = DEFAULT_PORT;
    }
    
    /* ------------------------------------------------------------------------ */
	/*							PROMPT FOR NICKNAME								*/
	/* ------------------------------------------------------------------------ */
    
    while(strlen(me.nickname) < 2){
        system("clear");
        printBar();
        printf("Enter your nickname: ");
        scanf("%s", me.nickname);
    }
    printf("Welcome %s\n", me.nickname);
    
    /* ------------------------------------------------------------------------ */
	/*							CONNECT TO SERVER								*/
	/* ------------------------------------------------------------------------ */
    
    printf("Contacting server...\n");
    
    // Identify player
    me.port = getRand(5001, 47000);
    msg.player = me;
    msg.action = ACTION_LOGIN;
    udp_snd(&msg, sizeof(Message), server_port, server_address);
    sock = udp_create_rcv(me.port);
    
    printf("waiting...\n");
    
    /* set the terminal to raw mode */
    tcgetattr(fileno(stdin), &orig_term_attr);
    memcpy(&new_term_attr, &orig_term_attr, sizeof(struct termios));
    new_term_attr.c_lflag &= ~(ECHO|ICANON);
    tcsetattr(fileno(stdin), TCSANOW, &new_term_attr);
    
    // Initialize threading
	pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    rc = pthread_create(&keyMaster, &attr, KeyMaster, (void*) NULL);
    if(rc){
        printf("Error: pthread_create() failed and returned %d\n", rc);
        exit(1);
    }
    
    // Loop on map display
    while(!killClient){
        rcv_map(sock, &map);
        system("clear");
        printMap(map);
    }
    
    /* ------------------------------------------------------------------------ */
	/*                              GRACEFUL EXIT								*/
	/* ------------------------------------------------------------------------ */
    // Restore termios struct
    tcsetattr(fileno(stdin), TCSANOW, &orig_term_attr);
    
    system("clear");
    printf("Bye bye.\n");
    
    udp_destroy(sock);
    rc = pthread_join(keyMaster, NULL);
    pthread_attr_destroy(&attr);
    pthread_exit(NULL);
}

/**
 * KeyMaster thread
 */
void *KeyMaster(void *nothing)
{
    Message msg;
    msg.player = me;
    int key;
    
    while(1){
        msg.action = -1;
        key = getchar();
        
        switch(key){
            case 119:   // [W] up
                msg.action = ACTION_MOVE;
                msg.direction = DIR_UP;
                break;
            case 97:    // [A] left
                msg.action = ACTION_MOVE;
                msg.direction = DIR_LEFT;
                break;
            case 115:   // [S] down
                msg.action = ACTION_MOVE;
                msg.direction = DIR_DOWN;
                break;
            case 100:   // [D] right
                msg.action = ACTION_MOVE;
                msg.direction = DIR_RIGHT;
                break;
            case 32:    // [Space] shoot
                msg.action = ACTION_SHOOT;
                break;
            case 27:    // [ESC] exit game
                killClient = 1;
                msg.action = ACTION_LEAVE;
                sleep(1); // wait for client to stop
                break;
            default:
                break;
        }
        if(msg.action >= 0){
            udp_snd(&msg, sizeof(Message), server_port, server_address);
            if(msg.action == ACTION_LEAVE){
                pthread_exit((void*) 0);
            }
        }
    }
    pthread_exit((void*) 0);
}

/**
* Receives all the pieces of data making a map 
* and reconstitutes the Map struct
*/
void rcv_map(int sock, Map* map)
{
    int head = 0, i;
    
    // Wait until we receive the header
    while(head != MAP_MSG_HEADER){
        udp_rcv(sock, &head, sizeof(int), NULL);
    }
    
    // Get map dimensions
    udp_rcv(sock, &(map->height), sizeof(int), NULL);
    udp_rcv(sock, &(map->width), sizeof(int), NULL);
    // Get map info
    udp_rcv(sock, &(map->wallCount), sizeof(int), NULL);
    udp_rcv(sock, &(map->spawnCount), sizeof(int), NULL);
    // Spawn locations
    udp_rcv(sock, map->spawn, sizeof(int), NULL);
    // Players & bullets
    udp_rcv(sock, map->players, sizeof(Player)*MAX_PLAYERS, NULL);
    udp_rcv(sock, &(map->playerCount), sizeof(int), NULL);
    
    // Map
    for(i=0; i<map->height; i++){
        udp_rcv(sock, map->map[i], sizeof(char)*MAX_MAP_WIDTH, NULL);
    }
}


/**
* Print a help message
*/
void help()
{
	printf("client - thomas.lextrait@gmail.com\n");
	printf("usage: client [-h|-k] [-a<server ip>] [-p<port>]\n");
	printf("\t-k\tKill the server.\n");
    printf("\t-a\tSpecify server IP address, otherwise will use %s\n", DEFAULT_ADDRESS);
    printf("\t-p\tSpecify server port, otherwise will use %d.\n", DEFAULT_PORT);
	printf("\t-h\tDisplay this help message.\n");
}
