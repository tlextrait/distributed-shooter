/*
 * Thomas Lextrait
 * tlextrait@wpi.edu
 * @file utils.c
 */

#include "utils.h"

#define VERSION_MAX_CHAR 	10
#define PATH_MAX_CHAR 		512

/**
* Creates a random number between min and max, inclusive
*/
int getRand(int min, int max)
{
    return min + rand()%(max-min);
}

/**
* Print color
*/
void printc(char* message, int color, int style)
{
    printf("%c[%d;%dm%s%c[%dm", 0x1B, style, color, message, 0x1B, 0);
}

/**
* Print a horizontal bar
*/
void printBar(){
	printf("------------------------------------------------\n");
}

/**
 * Checks if a file exists
 * @param name, name of file to be checked
 * @return 0 for false, 1 for true
 */
int file_exists(char* name)
{
	FILE* file = (FILE*)malloc(sizeof(FILE));
	file = fopen(name, "r");
	if(file==NULL){
		return 0;
	}else{
		fclose(file);
		return 1;
	}
}

/**
* Checks if given path is a directory or a file.
* @return 0 if directory, 1 if file, -1 if doesn't exist
*/
int pathExists(char* path)
{
	switch(isDir(path)){
		case 0:
		case 1:
			return 0;
		case -1:
		default:
			return -1;
	}
}

/**
* Checks if given path is a directory or a file.
* @return 0 if directory, 1 if file, -1 if doesn't exist
*/
int isDir(char* path)
{
	FILE *fp;
	
	if(opendir(path) == NULL){
		fp = fopen(path, "r");
		if(fp == NULL){
			// Doesn't exist
			return -1;
		}else{
			// It's a file
			fclose(fp);
			return 1;
		}
	}else{
		// It's a directory
		return 0;
	}
}

/**
* Takes a path and file name and combines them
*/
char* getPath(char* path, char* file)
{
	char* destination = (char*)malloc(sizeof(char)*PATH_MAX_CHAR);
	strncpy(destination, path, PATH_MAX_CHAR);
	
	// Append slash at the end of the path?
	if(
		destination[strlen(destination)-1] != '/' &&
		file[0] != '/'
	){
		destination = strcat(destination, "/");
	}
	
	// Append the file/folder path to the end
	destination = strcat(destination, file);
	
	return destination;
}

/**
* Trims the beginning of an absolute path until only
* the filename remains
*/
char* extractFilename(char* path)
{
	// Find location of last slash before filename
	int i, slashIndex=-1;
	for(i=strlen(path)-1; i>0 && slashIndex==-1; i--){
		if(path[i]=='/'){slashIndex = i;}
	}
	if(slashIndex >= 0){
		path+=slashIndex+1;
	}
	return path;
}

/**
* Trim the file extension out of the given filename
*/
void stripFileExtension(char* filename)
{
	int len = strlen(filename);
	int i = 0;
	while(i < len && *filename!='.'){
		filename++;
		i++;
	}
	while(i < len){
		*filename = '\0';
		filename++;
		i++;
	}
	filename -= len;
}

/**
* Checks if the subject starts with the search string
* return 1 for true, 0 for false
*/
int startsWith(char* subject, char* search)
{
	int count;
	int searchLen = strlen(search);
	if(strlen(subject) < strlen(search)){
		return 0;
	}else{
		for(count=0; count<searchLen; count++){
			if(*subject != *search){return 0;}
			subject++;
			search++;
		}
		subject -= count;
		search -= count;
		return 1;
	}
}

/** 
*	Copyright (C) 1989, 1990, 1991, 1992 Free Software Foundation, Inc.
*   Written by James Clark (jjc@jclark.com)
*	http://opensource.apple.com/source/groff/groff-10/groff/libgroff/itoa.c
*/
char* itoa(int i)
{
  /* Room for VERSION_MAX_CHAR digits, - and '\0' */
  static char buf[VERSION_MAX_CHAR + 2];
  char *p = buf + VERSION_MAX_CHAR + 1;	/* points to terminating '\0' */
  if (i >= 0) {
    do {
      *--p = '0' + (i % 10);
      i /= 10;
    } while (i != 0);
    return p;
  }else {			/* i < 0 */
    do {
      *--p = '0' - (i % 10);
      i /= 10;
    } while (i != 0);
    *--p = '-';
  }
  return p;
}
