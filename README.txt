CS4513 - Distributed Systems
Project 4

Thomas Lextrait
tlextrait@wpi.edu

Language:
C

Compilation:
Use make

Bugs:
Doesn't work when clients are on different computers, too much data loss.

Server:
The server fulfills all the requirements for the project:
- Moves players as requested, detects collisions with walls and other players
- Moves player bullets, up to 8 bullets per player (configurable up to 32)
- Detects collisions between bullets and walls/players
- Keeps track of health / score / ammo
- +2 score on kill, -1 score on death
- Up to 15 players (configurable up to 52)

Other notes on the server:
- Prints color messages representing live events
- Updates map every 100 milliseconds (configurable)
- Server shuts down when all players have exited

Game rules:
- No rounds
- Auto re-spawn on death, random valid and unoccupied location
- Default ammo 100
- Default weapon power 60
- Default health 100

usage: server [-h] [-p<port>]
	-p	Specify a port, otherwise will use 6666.
	-h	Display this help message.

Examples:
./server
./server -p 8763

Client:
usage: client [-h|-k] [-a<server ip>] [-p<port>]
	-k	Kill the server.
	-a	Specify a server ip address, otherwise will use '192.168.1.109'.
	-p	Specify a server port, otherwise will use 6666
	-h	Display this help message.

The client fulfills all the requirements for the project:
- Single interactive player
- Display of map, player list, scores, health, ammo
- Move up/down right/left with [W][A][S][D] keys
- Fire any direction, in the direction last moved with [Space]
- Exit gracefully with [ESC]

Additional features:
- Kill the server properly with ./client -k (will cause connected clients to freeze)
