/**
 * @file usock.h
 * @author Thomas Lextrait, tlextrait@wpi.edu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff /* should be in <netinet/in.h> */
#endif

/**
 * Create and return a listening UDP socket
 */
int udp_create_rcv(int port);

/**
 * Destroys given UDP socket
 */
int udp_destroy(int sock);

/**
 * Receive data from a UDP socket
 * @return bytes read
 */
int udp_rcv(int sock, void* data, size_t max_length, char* client_ip);

/**
 * Send data via given UDP socket
 * @return number of bytes sent
 */
int udp_snd(void* data, size_t length, int port, char* address);

