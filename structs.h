/**
* @file structs.h
* @author Thomas Lextrait
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_MAP_WIDTH       128
#define MAX_MAP_HEIGHT      64
#define MAX_SPAWN_LOCS      1024
#define MAX_PLAYERS			15

#define MAX_NAME_LEN        16
#define MAX_BULLETS         8

#define MAP_MSG_HEADER      123

/**
 * DIRECTIONS
 */
#define DIR_UP      1
#define DIR_DOWN    2
#define DIR_LEFT    3
#define DIR_RIGHT   4

/**
 * ACTIONS
 */
#define ACTION_MOVE     1
#define ACTION_SHOOT    2
#define ACTION_LOGIN    3
#define ACTION_LEAVE    4
#define ACTION_SHUT     5

/**
 * GAME SYMBOLS/ELEMENTS
 */
#define ELEMENT_BULLET  '*'
#define ELEMENT_AMMO    '$'
#define ELEMENT_HEALTH  '@'

#define GAME_DIRECTIONS "[W] go up; [A] go left; [S] go down; [D] go right; [Space] fire;\n[ESC] exit game."

typedef struct Pos{
    int x;
    int y;
}Pos;

typedef struct Bullet{
    Pos pos;
    int direction;
}Bullet;

typedef struct Player{
	char address[20];		// IP address of client
    int port;               // Client's port (assigned by client)
    
    char nickname[MAX_NAME_LEN];
    char symbol;
    
    Bullet bullets[MAX_BULLETS];
    int bulletCount;
    
    int ammo;           // assigned by server
    int health;         // assigned by server
    int power;          // assigned by server
    int score;          // assigned by server
    Pos pos;            // assigned by server
    int direction;      // assigned by server
}Player;

typedef struct Map{
	char map[MAX_MAP_HEIGHT][MAX_MAP_WIDTH];
    
    Player players[MAX_PLAYERS];
    int playerCount;
    
    Pos spawn[MAX_SPAWN_LOCS];
    int spawnCount;
    int wallCount;
    
    int width;
    int height;
}Map;

typedef struct Message{
    Player player;
    int action;     // see action macros
    int direction;  // direction of either shoot or move
}Message;

/**
 * Loads a map from the given filename
 */
void loadMap(Map* map, char* filename);

/**
 * Prints a map to screen
 */
void printMap(Map map);

/**
 * Returns 1 if this is a valid spawning location
 */
int isSpawnPos(Map map, Pos pos);

/**
 * Returns 1 if a player or bullet is at given location
 * -1 if invalid location
 */
int isOccupied(Map map, Pos pos);

/**
 * Returns 1 if given location is a valid moving position
 * (not a wall)
 */
int isValidPos(Map map, Pos pos);

/**
 * Returns 1 if the given positions are equivalent
 */
int samePos(Pos p1, Pos p2);

