#######################################
# Thomas Lextrait , tlextrait@wpi.edu
#######################################
CC=gcc
CFLAGS=-c -Wall
UTILSH=usock.h msock.h utils.h structs.h
UTILSO=usock.o msock.o utils.o structs.o
THREAD=-pthread
#######################################

all: client server

server: server.o $(UTILSO)
	$(CC) $(THREAD) server.o $(UTILSO) -o server
	
server.o: server.c server.h $(UTILSH)
	$(CC) $(CFLAGS) server.c

client: client.o $(UTILSO)
	$(CC) $(THREAD) client.o $(UTILSO) -o client

client.o: client.c client.h $(UTILSH)
	$(CC) $(CFLAGS) client.c
	
#######################################
# Structures
#######################################
structs.o: structs.c structs.h utils.h
	$(CC) $(CFLAGS) structs.c

#######################################
# Utilities
#######################################	
utils.o: utils.c utils.h
	$(CC) $(CFLAGS) utils.c
	
msock.o: msock.c msock.h
	$(CC) $(CFLAGS) msock.c
	
usock.o: usock.c usock.h
	$(CC) $(CFLAGS) usock.c

#######################################
# Clean up
#######################################
clean:
	rm -f *.o *.out client server

superclean:
	rm -f *.o *.out *~ client server
	