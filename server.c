/** 
 * @file server.c
 * @author Thomas Lextrait
 */

#include "usock.h"
#include "structs.h"
#include "utils.h"
#include "server.h"

/**
 * COMMUNICATIONS AND SYSTEM
 */
#define DEFAULT_ADDRESS     "192.168.1.109"
#define DEFAULT_PORT        6666

/**
 * MAPS AND RESOURCES
 */
#define MAP_FOLDER          "maps/"
#define DEFAULT_MAP         "default.txt"

/**
 * GAME RULES
 */
#define DEFAULT_AMMO        100
#define DEFAULT_HEALTH      100
#define DEFAULT_POWER       60
#define SCORE_KILL          2
#define SCORE_DEATH         -1
#define BULLET_MOVE         1       // positions a bullet should move per frame
#define UPDATE_DELAY        100     // game update delay in ms

/**
 * GLOBAL VARIABLES
 */
int killMaster = 0;       // Switch for killing the server
int sock;

Map                 map;                    // Gaming map

pthread_t 			gameMaster;       // GameMaster thread
pthread_mutex_t		mutexPrint;       // Mutex for printing to screen
pthread_mutex_t		mutexPlayers;     // Mutex for modifying player array
pthread_attr_t 		attr;             // thread attributes

int specifyPort = 0;
int server_port;


int main(int argc, char *argv[])
{
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	
	// Option stuff
	int c;
	extern int optind, opterr;
	extern char *optarg;
	
	// Socket stuff
	Message msg;
	
	// Thread stuff
	int rc;
	
    srand(time(NULL));
    
	/* ------------------------------------------------------------------------ */

	// Parse command line options
	while((c = getopt(argc, argv, "hp:")) != EOF) {
	    switch(c){
            case 'p': // specify port
                server_port = atoi(optarg);
                specifyPort = 1;
                break;
			default:
		    case '?': // opt error
		    case 'h': // help
		      	help();
				exit(1);
	    }
  	}
    
    if(!specifyPort){
        server_port = DEFAULT_PORT;
    }
    
    // Check map and map folder
    if(pathExists(MAP_FOLDER)!=0){
        printf("You need a folder containing game maps '%s'\n", MAP_FOLDER);
        exit(1);
    }
    if(pathExists(getPath(MAP_FOLDER, DEFAULT_MAP))<0){
        printf("The default map '%s' was not found in your map folder!\n", DEFAULT_MAP);
        exit(1);
    }
    
    // Open map
    loadMap(&map, getPath(MAP_FOLDER, DEFAULT_MAP));
    
    printBar();
    printf("Map loaded: %s\n", DEFAULT_MAP);
    printf("%dW x %dH\n%d spawn positions\n%d wall blocks\n", map.width, map.height, map.spawnCount, map.wallCount);
    printBar();

    printf("Waiting for a player to connect...\n");
    
    // Create socket
    sock = udp_create_rcv(server_port);
	
    // Wait for a user to connect
    while(msg.action != ACTION_LOGIN && 
          msg.action != ACTION_SHUT){
        
        udp_rcv(sock, &msg, sizeof(Message), msg.player.address);
        
        switch(msg.action){
            case ACTION_LOGIN:                
                if(map.playerCount < MAX_PLAYERS){
                    // add the player
                    addPlayer(msg.player);
                }else{
                    printc("Failed to add player, too many players!\n", RED, NOSTYLE);
                }
                break;
            case ACTION_SHUT:
                printc("Shutting down...\n", RED, NOSTYLE);
                udp_destroy(sock);
                exit(0);
            default:
                break;
        }
    }
    
    // Initialize threading
	pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_mutex_init(&mutexPrint, NULL);
	pthread_mutex_init(&mutexPlayers, NULL);
    
    // Start game master thread
    rc = pthread_create(&gameMaster, &attr, GameMaster, (void*) NULL);
    
    if(rc){
        printf("Error: pthread_create() failed and returned %d\n", rc);
        exit(1);
    }
    
    /* ------------------------------------------------------------------------ */
	/*                              PROCESS QUERIES								*/
	/* ------------------------------------------------------------------------ */
    
    while(!killMaster){
        
        udp_rcv(sock, &msg, sizeof(Message), msg.player.address);
        
        switch(msg.action){
            case ACTION_LOGIN:
                printc("# ", CYAN, NOSTYLE);
                printc(msg.player.nickname, CYAN, NOSTYLE);
                printc(" joined the server!\n", CYAN, NOSTYLE);
                // add the player
                addPlayer(msg.player);
                break;
            case ACTION_SHOOT:
                shoot(msg.player);
                break;
            case ACTION_MOVE:
                movePlayer(msg.player, msg.direction);
                break;
            case ACTION_LEAVE:
                removePlayer(msg.player);
                printc("# ", CYAN, NOSTYLE);
                printc(msg.player.nickname, CYAN, NOSTYLE);
                printc(" left the game\n", CYAN, NOSTYLE);
                // If zero players, then shut down
                if(map.playerCount <= 0){
                    printc("Shutting down...\n", RED, NOSTYLE);
                    killMaster = 1;
                }
                break;
            case ACTION_SHUT:
                printc("Shutting down...\n", RED, NOSTYLE);
                killMaster = 1;
                break;
        }
        
    }
    
    /* ------------------------------------------------------------------------ */
	/*                              GRACEFUL EXIT								*/
	/* ------------------------------------------------------------------------ */
    
    // Join game master thread
    rc = pthread_join(gameMaster, NULL);
    
	// Clear sockets
	udp_destroy(sock);
    
    // Clear thread stuff
    pthread_mutex_destroy(&mutexPrint);
	pthread_mutex_destroy(&mutexPlayers);
    pthread_attr_destroy(&attr);
    
    pthread_exit(NULL);
}

/**
* GameMaster thread
*/
void *GameMaster(void *nothing)
{
    int bytes, i, a;
    int header = MAP_MSG_HEADER;
    Pos newPos;
    
    while(!killMaster){
        
        // Send an update to each client
        for(i=0; i<map.playerCount; i++){
            
            // Send the header, indicating we're starting to send the map
            sendDataTo(&header, sizeof(int), map.players[i]);
            // Map dimensions
            sendDataTo(&(map.height), sizeof(int), map.players[i]);
            sendDataTo(&(map.width), sizeof(int), map.players[i]);
            // Map info
            sendDataTo(&(map.wallCount), sizeof(int), map.players[i]);
            sendDataTo(&(map.spawnCount), sizeof(int), map.players[i]);
            // Spawn locations
            sendDataTo(map.spawn, sizeof(int)*MAX_SPAWN_LOCS, map.players[i]);
            // Players & Bullets
            sendDataTo(map.players, sizeof(Player)*MAX_PLAYERS, map.players[i]);
            sendDataTo(&(map.playerCount), sizeof(int), map.players[i]);
            
            // Send entire map string
            for(a=0; a<map.height; a++){
                bytes = sendDataTo(map.map[a], sizeof(char)*MAX_MAP_WIDTH, map.players[i]);
            }
            
            // Update map, move all bullets etc.
            if(map.players[i].bulletCount > 0){
                for(a=0; a<map.players[i].bulletCount; a++){
                    newPos = map.players[i].bullets[a].pos;
                    switch(map.players[i].bullets[a].direction){
                        case DIR_UP:
                            newPos.y--;
                            break;
                        case DIR_DOWN:
                            newPos.y++;
                            break;
                        case DIR_RIGHT:
                            newPos.x++;
                            break;
                        case DIR_LEFT:
                            newPos.x--;
                            break;
                    }
                    
                    if(isValidPos(map, newPos)){
                        map.players[i].bullets[a].pos = newPos;
                        checkBullet(map.players[i], map.players[i].bullets[a]);
                    }else{
                        removeBullet(map.players[i], map.players[i].bullets[a]);
                    }
                }
            }
        }
        
        usleep(UPDATE_DELAY*1000);
    }
    
	pthread_exit((void*) 0);
}

/**
 * Sends given data to given player
 */
int sendDataTo(void* data, size_t size, Player p)
{
    return udp_snd(data, size, p.port, p.address);
}

/**
* Moves given player in the given direction
*/
void movePlayer(Player player, int dir)
{
    int i;
    Pos newPos;
    
    for(i=0; i<map.playerCount; i++){
        if(strcmp(map.players[i].nickname, player.nickname)==0){
            
            newPos = map.players[i].pos;
            map.players[i].direction = dir;
            
            switch(dir){
                case DIR_UP:
                    newPos.y--;
                    break;
                case DIR_DOWN:
                    newPos.y++;
                    break;
                case DIR_RIGHT:
                    newPos.x++;
                    break;
                case DIR_LEFT:
                    newPos.x--;
                    break;
            }
            
            if(isValidPos(map, newPos) && !isOccupied(map, newPos)){
                map.players[i].pos = newPos;
            }
        }
    }
}

/**
 * Makes given player shoot in player's direction
 */
void shoot(Player player)
{
    int i;
    Bullet bullet;
    

    for(i=0; i<map.playerCount; i++){
        if(strcmp(map.players[i].nickname, player.nickname)==0){
            
            bullet.pos = map.players[i].pos;
            bullet.direction = map.players[i].direction;
            
            if(map.players[i].ammo > 0){
            
                switch(bullet.direction){
                    case DIR_UP:
                        bullet.pos.y--;
                        break;
                    case DIR_DOWN:
                        bullet.pos.y++;
                        break;
                    case DIR_RIGHT:
                        bullet.pos.x++;
                        break;
                    case DIR_LEFT:
                        bullet.pos.x--;
                        break;
                }
                
                if(map.players[i].bulletCount < MAX_BULLETS && 
                   isValidPos(map, bullet.pos)){
                    map.players[i].bullets[map.players[i].bulletCount] = bullet;
                    map.players[i].bulletCount++;
                    map.players[i].ammo--;
                }
            }
        }
    }
}

/**
* Adds given player to the global list
*/
void addPlayer(Player player)
{
    Pos spawn;
    
    // Player symbol
    if(map.playerCount <= 26){
        player.symbol = (char)(65 + map.playerCount);
    }else{
        player.symbol = (char)(97 + map.playerCount - 26);
    }
    
    // Player location
    while(!isSpawnPos(map, spawn)){
        spawn.x = getRand(0, map.width-1);
        spawn.y = getRand(0, map.height-1);
    }
    
    player.pos = spawn;
    
    // Initialize player vars
    player.ammo = DEFAULT_AMMO;
    player.health = DEFAULT_HEALTH;
    player.power = DEFAULT_POWER;
    player.direction = 0;
    player.bulletCount = 0;
    
    // Address
    strcpy(player.address, DEFAULT_ADDRESS);
    
    printc("# [", CYAN, NOSTYLE);
    printc(&(player.symbol), CYAN, NOSTYLE);
    printc("] ", CYAN, NOSTYLE);
    printc(player.nickname, CYAN, NOSTYLE);
    printc(" joined the server!\n", CYAN, NOSTYLE);
    
    printf("# %s at %d:%d with %c\n", player.nickname, player.pos.x, player.pos.y, player.symbol);
    
    pthread_mutex_lock(&mutexPlayers);
    map.players[map.playerCount] = player;
    map.playerCount++;
    pthread_mutex_unlock(&mutexPlayers);
}

/**
* Removes player from global list
*/
void removePlayer(Player player)
{
    int i = 0;
    pthread_mutex_lock(&mutexPlayers);
    while(strcmp(map.players[i].nickname, player.nickname)!=0){i++;}
    while(i<map.playerCount-1){
        map.players[i] = map.players[i+1];
        i++;
    }
    map.playerCount--;
    pthread_mutex_unlock(&mutexPlayers);
}

/**
* Removes a bullet from the given player
*/
void removeBullet(Player player, Bullet bullet)
{
    int i=0, a=0;
    pthread_mutex_lock(&mutexPlayers);
    
    // Find the player
    while(strcmp(map.players[i].nickname, player.nickname)!=0){i++;}

    // Find the bullet
    while(!samePos(map.players[i].bullets[a].pos, bullet.pos)){a++;}
    while(a<player.bulletCount-1){
        map.players[i].bullets[a] = map.players[i].bullets[a+1];
        a++;
    }
    map.players[i].bulletCount--;

    pthread_mutex_unlock(&mutexPlayers);
}

/**
* Checks if given player's bullet killed another player
*/
int checkBullet(Player player, Bullet bullet)
{
    int i=0, a=0;
    Pos spawn;
    
    for(i=0; i<map.playerCount; i++){
        if(samePos(map.players[i].pos, bullet.pos)){
            
            printc("# ", RED, NOSTYLE);
            printc(player.nickname, RED, NOSTYLE);
            
            map.players[i].health -= player.power;
            if(map.players[i].health <= 0){
                // Kill player - new random position
                while(!isSpawnPos(map, spawn)){
                    spawn.x = getRand(0, map.width-1);
                    spawn.y = getRand(0, map.height-1);
                }
                map.players[i].pos = spawn;
                map.players[i].health = DEFAULT_HEALTH;
                map.players[i].ammo = DEFAULT_AMMO;
                map.players[i].score += SCORE_DEATH;
                
                printc(" killed ", RED, NOSTYLE);
                
                // Find killer and add score
                while(strcmp(map.players[a].nickname, player.nickname)!=0){a++;}
                map.players[a].score += SCORE_KILL;
            }else{
                printc(" hit ", RED, NOSTYLE);
            }
            printc(map.players[i].nickname, RED, NOSTYLE);
            printf("\n");
            
            // Remove bullet
            removeBullet(player, bullet);
            return 1;
        }
    }
    return 0;
}

/**
* printm is a thread-safe printing function
*/
void printm(char* message)
{
	pthread_mutex_lock(&mutexPrint);
	printf("%s", message);
	pthread_mutex_unlock(&mutexPrint);
}

/**
* Print a help message
*/
void help()
{
	printf("server - thomas.lextrait@gmail.com\n");
	printf("usage: server [-h] [-p<port>]\n");
	printf("\t-p\tSpecify a port, otherwise will use %d.\n", DEFAULT_PORT);
	printf("\t-h\tDisplay this help message.\n");
}

