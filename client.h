/**
* @file client.h
* @author Thomas Lextrait
*/

#include <stdio.h>
#include <stdlib.h> 	// system(clear)
#include <string.h>
#include <unistd.h>		// getopt()
#include <time.h>

#include <termios.h>    // manipulate input stream

#include <pthread.h>

/**
 * KeyMaster thread
 */
void *KeyMaster(void *nothing);

/**
 * Receives all the pieces of data making a map 
 * and reconstitutes the Map struct
 */
void rcv_map(int sock, Map* map);

/**
* Print a help message
*/
void help();
