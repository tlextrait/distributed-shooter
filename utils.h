/*
 * Thomas Lextrait
 * tlextrait@wpi.edu
 * @file utils.h
 */

#include <stdio.h>		// remove()
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>		// opendir()
#include <sys/stat.h> 	// mkdir()
#include <sys/types.h> 	// mkdir()
#include <unistd.h>		// getcwd()
#include <string.h>

/**
 * COLOR PRINTING
 */
#define NOSTYLE 0
#define BOLD    1
#define BLINK   5

#define BLACK   30
#define RED     31
#define GREEN   32
#define YELLOW  33
#define BLUE    34
#define MAGENTA 35
#define CYAN    36
#define WHITE   37

/**
 * Creates a random number between min and max, inclusive
 */
int getRand(int min, int max);

/**
 * Print color
 */
void printc(char* message, int color, int style);

/**
* Print a horizontal bar
*/
void printBar();

/* Checks if a file exists
 */
int file_exists(char* name);

/**
* Checks if given path is a directory or a file.
* @return 0 if directory, 1 if file, -1 if doesn't exist
*/
int pathExists(char* path);

/**
* Checks if given path is a directory or a file.
* @return 0 if directory, 1 if file, -1 if doesn't exist
*/
int isDir(char* path);

/**
* Takes a path and file name and combines them
*/
char* getPath(char* path, char* file);

/**
* Trims the beginning of an absolute path until only
* the filename remains
*/
char* extractFilename(char* path);

/**
* Trim the file extension out of the given filename
*/
void stripFileExtension(char* filename);

/**
* Checks if the subject starts with the search string
* return 1 for true, 0 for false
*/
int startsWith(char* subject, char* search);

/** 
*	Copyright (C) 1989, 1990, 1991, 1992 Free Software Foundation, Inc.
*   Written by James Clark (jjc@jclark.com)
*	http://opensource.apple.com/source/groff/groff-10/groff/libgroff/itoa.c
*/
char* itoa(int i);
